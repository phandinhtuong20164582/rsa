package squareAndMultiply;

import java.math.BigInteger;

public class SquareAndMultiply {
	public static BigInteger squareAndMultiply(BigInteger x, BigInteger e, BigInteger n){
		String eInBinary = e.toString(2);
		BigInteger z = new BigInteger("1");
		for (int i =0; i<eInBinary.length();i++){
			z = z.multiply(z);
			z = z.mod(n);
			if (eInBinary.charAt(i) == '1') {
				z = z.multiply(x);
				z = z.mod(n);
			}
			}
		if (z.compareTo(new BigInteger("0")) < 0) z = z.add(n);
		return z;
	}
}
