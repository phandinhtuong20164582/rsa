package modular;

import java.math.BigInteger;

public class ModularMultiInverse {
	public static BigInteger modInverse(BigInteger e, BigInteger phi) 
	    { 
		BigInteger m0 = phi; 
		BigInteger y = new BigInteger("0");
		BigInteger x = new BigInteger("1"); 
	        if (phi.compareTo(new BigInteger("1")) == 0) 
	            return new BigInteger("0"); 
	        while (e.longValue() > 1) 
	        { 
	            // q is quotient
	        	if (phi.compareTo(new BigInteger("0")) == 0){
	        		break;
	        	}
	        	BigInteger q = e.divide(phi); 
	        	BigInteger t = phi; 
	            // m is remainder now, process 
	            // same as Euclid's algo 
	            phi = e.mod(phi); 
	            e = t; 
	            t = y; 
	  
	            // Update x and y 
	            q = q.multiply(y);
	            y = x.subtract(q);
	            x = t; 
	        } 
	        // Make x positive 
	        if (x.longValue() < 0) 
	        	x = x.add(m0);
	        if (phi.compareTo(new BigInteger("0")) == 0){
        		return new BigInteger("-1");
        	}
	        return x; 
	    } 
}
