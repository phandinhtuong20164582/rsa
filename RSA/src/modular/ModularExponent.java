package modular;

public class ModularExponent {
	public long power(long A, long B, long C) {
		// Base cases  
	    if (A == 0)  
	        return 0;  
	    if (B == 0)  
	        return 1;  
	    // If B is even  
	    long y;  
	    if (B % 2 == 0) 
	    {  
	        y = power(A, B / 2, C);  
	        y = (y * y) % C;  
	    }  
	    // If B is odd  
	    else 
	    {  
	        y = A % C;  
	        y = (y * power(A, B - 1,  
	                             C) % C) % C;  
	    }  
	      
	    return (int)((y + C) % C);  
	}
}
