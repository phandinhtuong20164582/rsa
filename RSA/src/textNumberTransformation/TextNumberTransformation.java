package textNumberTransformation;

import java.math.BigInteger;

import findPrivateKey.FindD;
import findPrivateKey.FindPQ;
import findPrivateKey.object.PandQ;
import squareAndMultiply.SquareAndMultiply;

public class TextNumberTransformation {
	static BigInteger e = new BigInteger("65729");
	static BigInteger n = new BigInteger("2190900097").multiply(new BigInteger("2000000099"));
	static BigInteger d;
	public static BigInteger getE() {
		return e;
	}
	public void setE(BigInteger e) {
		TextNumberTransformation.e = e;
	}
	public static BigInteger getN() {
		return n;
	}
	public void setN(BigInteger n) {
		TextNumberTransformation.n = n;
	}
	public static BigInteger getD() {
		return d;
	}
	public static void setD(BigInteger d) {
		TextNumberTransformation.d = d;
	}
	public static String[] getWordArray(String text){
		String[] wordArray = text.split(" ");
		return wordArray;
	}
	public static String encodeWord(String plainWord){
		BigInteger plainWordInNumber = WordNumberTransformation.wordToNumber(plainWord);
		BigInteger cipherWordInNumber = SquareAndMultiply.squareAndMultiply(plainWordInNumber, e, n);
		String cipherWord = WordNumberTransformation.numberToWord(cipherWordInNumber);
		return cipherWord;
	}
	public static String decodeWord(String cipherWord){
		BigInteger cipherWordInNumber = WordNumberTransformation.wordToNumber(cipherWord);
		BigInteger plainWordInNumber = SquareAndMultiply.squareAndMultiply(cipherWordInNumber, d, n);
		String plainWord = WordNumberTransformation.numberToWord(plainWordInNumber);
		return plainWord;
	}
	public static String[] encodeWordArray(String[] plainWordArray){
		String[] cipherWordArray = new String[plainWordArray.length];
		for (int i = 0; i< cipherWordArray.length;i++){
			cipherWordArray[i] = encodeWord(plainWordArray[i]);
		}
		return cipherWordArray;
	}
	public static String[] decodeWordArray(String[] cipherWordArray){
		String[] plainWordArray = new String[cipherWordArray.length];
		FindPQ findPQ = new FindPQ();
		PandQ pAndq = findPQ.findPandQ(n);
		BigInteger p = new BigInteger(String.valueOf(pAndq.getP()));
		BigInteger q = new BigInteger(String.valueOf(pAndq.getQ()));
		if (p.longValue() == 0){
			String[] error = new String[1];
			error[0] = "ERROR: CANNOT FIND P AND Q!";
			return error;
		}
		d = FindD.findD(p, q, e);
		if (d.compareTo(new BigInteger("-1")) == 0){
			String[] error = new String[1];
			error[0] = "ERROR: CANNOT FIND D!";
			return error;
		}
		for (int i = 0; i<plainWordArray.length;i++){
			plainWordArray[i] = decodeWord(cipherWordArray[i]);
		}
		return plainWordArray;
	}
	public String encodeText(String plainText){
		String[] plainWordArray = getWordArray(plainText);
		String[] cipherWordArray = encodeWordArray(plainWordArray);
		String cipherText = "";
		for (int i = 0; i < cipherWordArray.length;i++){
			cipherText += cipherWordArray[i];
			if( i != cipherWordArray.length-1)
				cipherText += " ";
		}
		return cipherText;
	}
	public String decodeText(String cipherText){
		String[] cipherWordArray = getWordArray(cipherText);
		String[] plainWordArray = decodeWordArray(cipherWordArray);
		
		String plainText = "";
		for (int i = 0; i<plainWordArray.length;i++){
			plainText += plainWordArray[i];
			if (i!= plainWordArray.length-1)
				plainText += " ";
		}
		return plainText;
	}
}
