package textNumberTransformation;

import java.math.BigInteger;

public class WordNumberTransformation {
	public static BigInteger sumOfPower(int numberOfChar){ // a number to check the length of text
		BigInteger sum = new BigInteger("0");
		while(numberOfChar >= 0){
			sum = sum.add((new BigInteger(Long.toString((long) Math.pow(26, (numberOfChar))))));
			numberOfChar--;
		}
		return sum;
	}
	public static int findNumberOfChar(BigInteger textInNumber){
		int numberOfChar = 0;
		while(numberOfChar < 30){
			if (textInNumber.compareTo(sumOfPower(numberOfChar)) < 0){
				return numberOfChar;
			}
			numberOfChar++;
		}
		return numberOfChar;
	}
	public static int findMultiplicand(int power, BigInteger textInNumber){
		int multiplicand = 26;
		BigInteger checkPower = new BigInteger("-1");
		if (power == 0) return (int) textInNumber.longValue();
		do {
			checkPower =  textInNumber.subtract((new BigInteger(Long.toString((long) Math.pow(26, (power))))).multiply(new BigInteger(String.valueOf(multiplicand))));
			checkPower = checkPower.subtract(sumOfPower(power-1));
			multiplicand--;
		}while(checkPower.compareTo(new BigInteger("0")) < 0);
		return (multiplicand+1);
	}
	public static String numberToWord(BigInteger wordInNumber){
		int numberOfChar = findNumberOfChar(wordInNumber);
		int power = numberOfChar - 1;
		int multiplicand = 0;
		char letter;
		String returnText = "";
		while (numberOfChar > 0){
			multiplicand = findMultiplicand(power, wordInNumber);
			letter = (char) (multiplicand -1 + 'a');
			returnText += letter;
			BigInteger letterInNumber = (new BigInteger(Long.toString((long) Math.pow(26, (power))))).multiply(new BigInteger(String.valueOf(multiplicand)));
			wordInNumber = wordInNumber.subtract(letterInNumber);
			numberOfChar--;
			power--;
		}
		return returnText;
	}
	public static BigInteger wordToNumber(String word){
		BigInteger textInNumber = new BigInteger("0");
		BigInteger letterInNumber = new BigInteger("0");
		int letterIndex;
		char letter;
		for (int i = word.length()-1; i>=0;i--){
			letter = word.charAt(i);
			letterIndex = (int)letter - (int)'a' +1;
			letterInNumber = (new BigInteger(Long.toString((long) Math.pow(26, word.length()-i-1)))).multiply(new BigInteger(String.valueOf(letterIndex)));;
			textInNumber = textInNumber.add(letterInNumber);
		}
		return textInNumber;
	}
}
