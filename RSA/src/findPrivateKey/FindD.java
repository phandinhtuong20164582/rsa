package findPrivateKey;

import java.math.BigInteger;

import modular.ModularMultiInverse;

public class FindD {
	public static BigInteger findD(BigInteger p, BigInteger q, BigInteger e) {
		BigInteger d = new BigInteger("0");
		BigInteger pMinusOne = (p.subtract(new BigInteger("1")));
		BigInteger qMinusOne = (q.subtract(new BigInteger("1")));
		BigInteger phi = pMinusOne.multiply(qMinusOne);
		d = ModularMultiInverse.modInverse(e, phi);
		return d;
	}
}
