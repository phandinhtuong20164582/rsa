package findPrivateKey;

import java.math.BigInteger;

import findPrivateKey.object.PandQ;
import main.Main;

public class FindPQ {
	public static BigInteger sqrt(BigInteger x) {
	    BigInteger div = BigInteger.ZERO.setBit(x.bitLength()/2);
	    BigInteger div2 = div;
	    // Loop until we hit the same value twice in a row, or wind
	    // up alternating.
	    for(;;) {
	        BigInteger y = div.add(x.divide(div)).shiftRight(1);
	        if (y.equals(div) || y.equals(div2))
	            return y;
	        div2 = div;
	        div = y;
	    }
	}
	static boolean isPrime(long n) {
	    if(n < 2) return false;
	    if(n == 2 || n == 3) return true;
	    if(n%2 == 0 || n%3 == 0) return false;
	    long sqrtN = (long)Math.sqrt(n)+1;
	    for(long i = 6L; i <= sqrtN; i += 6) {
	        if(n%(i-1) == 0 || n%(i+1) == 0) return false;
	    }
	    return true;
	}
	public PandQ findPandQ(BigInteger n){
		// p = a+x;
		// q = a-x;
		//a = (p+q)/2;
		//n = (a+x)(a-x) = a^2 - x^2
		//
		//find sqrt of n, round it to next int and use as a starting point to find a, a++
		//
		//for each a, cal x = sqrt(a^2 - n), if x is int then done
		//
		BigInteger p = new BigInteger("0");
		BigInteger q = new BigInteger("0");
		
		BigInteger sqrtN = sqrt(n);
		BigInteger a = sqrtN.add(new BigInteger("1"));
		BigInteger i = new BigInteger("0");
		double x;
		BigInteger xInLong = new BigInteger("0");
		BigInteger aInLong = new BigInteger("0");
		long startTime = System.currentTimeMillis()/1000;
		long endTime   = System.currentTimeMillis();
		long timeRunning = 0;
		long oldTotalTime = -1;
		for (i = i.add(a); p.longValue() == 0 && i.compareTo(n) < 0 ;i= i.add(new BigInteger("1"))){
			endTime   = System.currentTimeMillis()/1000;
			timeRunning = endTime - startTime;
			if (timeRunning % 1 == 0){
				if (timeRunning != oldTotalTime){	
					Main.window.frame.setTitle(String.valueOf(timeRunning)+"s");
				}
				oldTotalTime = timeRunning;
			}
			BigInteger iSquare = i.multiply(i);
			x = sqrt(iSquare.subtract(n)).doubleValue();
			if (x%1 == 0) {
				xInLong = new BigInteger(String.valueOf((int)x));
				aInLong = aInLong.add(i);
				p = aInLong.add(xInLong);
				if (isPrime(p.longValue())){
					q = aInLong.subtract(xInLong);
					if (isPrime(q.longValue())){
						BigInteger newN = p.multiply(q);
						if (newN.equals(n)){
						}else{
							p = new BigInteger("0");
							aInLong = new BigInteger("0");
						}
					}else{
						p = new BigInteger("0");
						aInLong = new BigInteger("0");
					}
				}else {
					p = new BigInteger("0");
					aInLong = new BigInteger("0");
				}
				
			}
		}
		if (i.compareTo(n) < 0){
			p = aInLong.add(xInLong);
			q = aInLong.subtract(xInLong);
		}
		PandQ pAndq = new PandQ(p.longValue(),q.longValue());
		return pAndq;
	}
}
