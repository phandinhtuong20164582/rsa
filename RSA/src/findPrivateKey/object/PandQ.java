package findPrivateKey.object;

public class PandQ {
	long p;
	long q;
	public long getP() {
		return p;
	}
	public void setP(long p) {
		this.p = p;
	}
	public long getQ() {
		return q;
	}
	public void setQ(long q) {
		this.q = q;
	}
	public PandQ(long p, long q) {
		super();
		this.p = p;
		this.q = q;
	}
}
