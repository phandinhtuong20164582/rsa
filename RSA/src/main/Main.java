package main;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigInteger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import textNumberTransformation.TextNumberTransformation;


public class Main {

	public JFrame frame;
	private JTextField txtFldInputText;
	private JTextField txtFldInputE;
	private JTextField txtFldInputN;
	private JTextField txtFldOutput;
	JLabel lblRunTime = new JLabel("");
	public static Main window = new Main();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
					window.frame.setVisible(true);
				
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	public static boolean isNumeric(String input) {
        boolean isNumeric = input.matches("[0-9]+");
        return isNumeric;
    }
	public static boolean isLetter(String input){
		boolean isLetter = input.matches("[a-z]+");
		return isLetter;
	}
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 1437, 675);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txtFldInputText = new JTextField();
		txtFldInputText.setFont(new Font("DialogInput", Font.BOLD, 30));
		txtFldInputText.setBounds(12, 54, 1380, 113);
		frame.getContentPane().add(txtFldInputText);
		txtFldInputText.setColumns(10);
		
		JLabel lblInputText = new JLabel("Input text:");
		lblInputText.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblInputText.setBounds(28, 13, 241, 42);
		frame.getContentPane().add(lblInputText);
		
		JLabel lblEnterE = new JLabel("Input e:");
		lblEnterE.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblEnterE.setBounds(28, 194, 161, 42);
		frame.getContentPane().add(lblEnterE);
		
		JLabel lblInputN = new JLabel("Input n:");
		lblInputN.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblInputN.setBounds(729, 194, 161, 42);
		frame.getContentPane().add(lblInputN);
		
		txtFldInputE = new JTextField();
		txtFldInputE.setFont(new Font("DialogInput", Font.BOLD, 30));
		txtFldInputE.setColumns(10);
		txtFldInputE.setBounds(12, 235, 612, 80);
		frame.getContentPane().add(txtFldInputE);
		
		txtFldInputN = new JTextField();
		txtFldInputN.setFont(new Font("DialogInput", Font.BOLD, 30));
		txtFldInputN.setColumns(10);
		txtFldInputN.setBounds(729, 235, 612, 80);
		frame.getContentPane().add(txtFldInputN);
		
		final JLabel lblTimeToFind = new JLabel("Time to find P and Q: ");
		lblTimeToFind.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblTimeToFind.setBounds(697, 360, 396, 33);
		frame.getContentPane().add(lblTimeToFind);
		lblTimeToFind.setVisible(false);
		lblRunTime.setVisible(false);
		
		
		JButton btnEncrypt = new JButton("Encrypt");
		btnEncrypt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				frame.getContentPane().setVisible(false);
				if(txtFldInputText.getText().equals("")){
					String nullText = "ERROR: PLEASE INPUT TEXT!";
					txtFldOutput.setText(nullText);
				}else if (txtFldInputE.getText().equals("")){
					String nullText = "ERROR: PLEASE INPUT E!";
					txtFldOutput.setText(nullText);
				}else if (txtFldInputN.getText().equals("")){
					String nullText = "ERROR: PLEASE INPUT N!";
					txtFldOutput.setText(nullText);
				}else if (!isNumeric(txtFldInputE.getText())){
					String nullText = "ERROR: INVALID E!";
					txtFldOutput.setText(nullText);
				}
				else if (!isNumeric(txtFldInputN.getText())){
					String nullText = "ERROR: INVALID N!";
					txtFldOutput.setText(nullText);
				}
				else if (!isLetter(txtFldInputText.getText())){
					String nullText = "ERROR: INVALID TEXT, PLEASE INPUT LOWERCASE LETTER WITH SPACE ONLY!";
					txtFldOutput.setText(nullText);
				}
				else{
					String plainText = txtFldInputText.getText();
					TextNumberTransformation textNumberTransformation = new TextNumberTransformation();
					textNumberTransformation.setE(new BigInteger(txtFldInputE.getText()));
					textNumberTransformation.setN(new BigInteger(txtFldInputN.getText()));
					String longCipherText = textNumberTransformation.encodeText(plainText);
					txtFldOutput.setText(longCipherText);
				}
				lblRunTime.setVisible(false);
				lblTimeToFind.setVisible(false);
				frame.getContentPane().setVisible(true);
			}
		});
		btnEncrypt.setFont(new Font("DialogInput", Font.BOLD, 30));
		btnEncrypt.setBounds(99, 352, 186, 49);
		frame.getContentPane().add(btnEncrypt);
		
		JLabel lblTime = new JLabel("time");
		lblTime.setFont(new Font("DialogInput", Font.BOLD, 30));
		lblTime.setBounds(421, 465, 118, 33);
		
		
		JButton btnDecrypt = new JButton("Decrypt");
		btnDecrypt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				frame.getContentPane().setVisible(false);
				if(txtFldInputText.getText().equals("")){
					String nullText = "ERROR: PLEASE INPUT TEXT!";
					txtFldOutput.setText(nullText);
				}else if (txtFldInputE.getText().equals("")){
					String nullText = "ERROR: PLEASE INPUT E!";
					txtFldOutput.setText(nullText);
				}else if (txtFldInputN.getText().equals("")){
					String nullText = "ERROR: PLEASE INPUT N!";
					txtFldOutput.setText(nullText);
				}else if (!isNumeric(txtFldInputE.getText())){
					String nullText = "ERROR: INVALID E!";
					txtFldOutput.setText(nullText);
				}
				else if (!isNumeric(txtFldInputN.getText())){
					String nullText = "ERROR: INVALID N!";
					txtFldOutput.setText(nullText);
				}
				else if (!isLetter(txtFldInputText.getText())){
					String nullText = "ERROR: INVALID TEXT, PLEASE INPUT LOWERCASE LETTER WITH SPACE ONLY!";
					txtFldOutput.setText(nullText);
				}
				else{
					String cipherText = txtFldInputText.getText();
					TextNumberTransformation textNumberTransformation = new TextNumberTransformation();
					textNumberTransformation.setE(new BigInteger(txtFldInputE.getText()));
					textNumberTransformation.setN(new BigInteger(txtFldInputN.getText()));
					
					String newLongPlainText = textNumberTransformation.decodeText(cipherText);
					
					String time = frame.getTitle();
					
					lblRunTime.setText(time);
					lblRunTime.setFont(new Font("DialogInput", Font.BOLD, 30));
					lblRunTime.setBounds(1100, 360, 121, 33);
					frame.getContentPane().add(lblRunTime);
					lblRunTime.setVisible(true);
					
					lblTimeToFind.setVisible(true);
					txtFldOutput.setText(newLongPlainText);
				}
				
				
				frame.getContentPane().setVisible(true);
			}
		});
		
		btnDecrypt.setFont(new Font("DialogInput", Font.BOLD, 30));
		btnDecrypt.setBounds(369, 352, 186, 49);
		frame.getContentPane().add(btnDecrypt);
		
		JLabel label = new JLabel("Output:");
		label.setFont(new Font("DialogInput", Font.BOLD, 30));
		label.setBounds(28, 427, 161, 42);
		frame.getContentPane().add(label);
		
		txtFldOutput = new JTextField();
		txtFldOutput.setFont(new Font("DialogInput", Font.BOLD, 30));
		txtFldOutput.setColumns(10);
		txtFldOutput.setBounds(12, 469, 1380, 113);
		frame.getContentPane().add(txtFldOutput);
		
	}

}
